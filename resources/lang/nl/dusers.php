<?php
return [
    'new' => 'Nieuw',
    'roles' => 'Rollen',
    'permissions' => 'Bevoegdheden',
    'new_permission' => 'Nieuwe bevoegdheid',
    'new_role' => 'Nieuwe rol',
    'edit' => 'Bewerk',
    'cancel' => 'Annuleer',
    'save' => 'Opslaan',
    'save_exit' => 'Opslaan en sluiten',
    'delete' => 'Verwijder',
    'no_entries_in_table' => 'Er zijn nog geen items ingevoerd',
    'users' => [
        'management' => 'Gebruikersbeheer',
        'users' => 'Gebruikers',
        'new' => 'Nieuwe gebruiker',
        'edit' => 'Bewerk gebruiker',
        'name' => 'Naam',
        'email' => 'E-mailadres',
        'no_items' => 'Er zijn nog geen gebruikers ingevoerd.',
        'delete_sure' => 'Weet je zeker dat je deze gebruiker wilt verwijderen?',
        'password' => 'Wachtwoord',
        'confirm_password' => 'Bevestig het wachtwoord'
    ],
    'invalid' => [
        'password' => 'Het wachtwoord voldoet niet aan de eisen. Het moet minimaal 8 karakters lang zijn en minimaal een hoofdletter, een kleine letter en een getal of vreemd teken bevatten (!$%&*#?)'
    ],
    'form' => [
        'item_created_successfully' => 'Het item is met succes aangemaakt',
        'item_updated_successfully' => 'Het item is met succes aangepast',
        'delete_sure' => 'Weet je zeker dat je dit item wilt verwijderen?',
        'item_deleted_successfully' => 'Het item is met succes verwijderd',
        'item_deletion_failed' => 'Er ging iets mis bij het verwijderen van dit item',
    ]
];

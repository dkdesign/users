@extends('dusers::layouts.base')

@section('content_header')
    <h1>@lang('dusers::dusers.new_role')</h1>
@stop

@section('main')
    <div class="row">
        <div class="col-lg-6">

            <form method="post" action="{{ route('dusers.roles.store') }}">
                @csrf
                @include('dusers::snippets.buttons')

                <x-dcomp-box :title="__('dusers::dusers.new_role')"  :collapsible="false">
                    <x-dcomp-input-string
                        name="name"
                        label="{{ ucfirst(__('dusers::dusers.users.name')) }}"
                        value="{{ old('name') }}"
                        :required="true"
                        :show-errors="true"
                        :errors="($errors)?? []"/>

                    <x-dcomp-multiselect
                        name="permission[]"
                        label="{{ ucfirst(__('dusers::dusers.permissions')) }}"
                        :options="$permissions"
                        :selected="old('permissions[]')"
                        :show-errors="true"
                        :errors="($errors)?? []"/>

                </x-dcomp-box>
            </form>

        </div>
    </div>
@stop

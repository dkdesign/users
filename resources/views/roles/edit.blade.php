@extends('dusers::layouts.base')

@section('content_header')
    <h1>{{ $role->name }}</h1>
@stop

@section('main')
    <div class="row">
        <div class="col-lg-6">

            <form method="post" action="{{ route('dusers.roles.update', [$role->id]) }}">
                <input name="_method" type="hidden" value="PUT">
                @csrf
                @include('dusers::snippets.buttons')

                <x-dcomp-box :title="__('dusers::dusers.edit')"  :collapsible="false">
                    <x-dcomp-input-string
                        name="name"
                        label="{{ ucfirst(__('dusers::dusers.users.name')) }}"
                        value="{{ old('name', $role->name) }}"
                        :required="true"
                        :show-errors="true"
                        :errors="($errors)?? []"/>

                    <x-dcomp-multiselect
                        name="permissions[]"
                        label="{{ ucfirst(__('dusers::dusers.permissions')) }}"
                        :options="$permissions"
                        :selected="old('permissions[]', $role->permissions()->pluck('name', 'name')->toArray())"
                        :show-errors="true"
                        :errors="($errors)?? []"/>
                </x-dcomp-box>

            </form>
        </div>
    </div>
@stop

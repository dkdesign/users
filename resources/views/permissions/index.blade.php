<x-dcomp-box :title="__('dusers::dusers.roles')" :collapsible="false">
    <a href="{{ route('dusers.roles.create') }}" class="mb-2 btn btn-xs btn-success">@lang('dusers::dusers.new')</a>

    <table id="rolesTable" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>@lang('dusers::dusers.users.name')</th>
            <th>@lang('dusers::dusers.permissions')</th>
            <th style="width: 150px;">&nbsp;</th>
        </tr>
        </thead>

        <tbody>
        @if (count($roles) > 0)
            @foreach ($roles as $role)
                @if($role->name != 'super-user')
                    <tr data-entry-id="{{ $role->id }}">
                        <td>{{ $role->name }}</td>
                        <td>
                            @foreach ($role->permissions()->pluck('name') as $permission)
                                <span class="badge badge-secondary">{{ $permission }}</span>
                            @endforeach
                        </td>
                        <td>
                            <a href="{{ route('dusers.roles.edit',[$role->id]) }}" class="btn btn-xs btn-info">@lang('dusers::dusers.edit')</a>
                            <form style="display: inline-block;" method="post" action="{{ route('dusers.roles.destroy', $role->id) }}" onsubmit="return confirm('{{ __('dusers::dusers.form.delete_sure') }}');">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" value="@lang('dusers::dusers.delete')" class="btn btn-xs btn-danger">
                            </form>
                        </td>
                    </tr>
                @endif
            @endforeach
        @else
            <tr>
                <td colspan="3">@lang('dusers::dusers.no_entries_in_table')</td>
            </tr>
        @endif
        </tbody>
    </table>
</x-dcomp-box>

<x-dcomp-box :title="__('dusers::dusers.permissions')" :collapsible="false">
    <a href="{{ route('dusers.permissions.create') }}" class="mb-2 btn btn-xs btn-success">@lang('dusers::dusers.new')</a>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>@lang('dusers::dusers.users.name')</th>
            <th style="width: 150px;">&nbsp;</th>

        </tr>
        </thead>

        <tbody>
        @if (count($permissions) > 0)
            @foreach ($permissions as $permission)
                <tr data-entry-id="{{ $permission->id }}">
                    <td>{{ $permission->name }}</td>
                    <td>
                        <a href="{{ route('dusers.permissions.edit',[$permission->id]) }}" class="btn btn-xs btn-info">Bewerk</a>
                        <form style="display: inline-block;" method="post" action="{{ route('dusers.permissions.destroy', $permission->id) }}" onsubmit="return confirm('{{ __('dusers::dusers.form.delete_sure') }}');">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" value="@lang('dusers::dusers.delete')" class="btn btn-xs btn-danger">
                        </form>
                    </td>

                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="3">@lang('dusers::dusers.no_entries_in_table')</td>
            </tr>
        @endif
        </tbody>
    </table>
</x-dcomp-box>

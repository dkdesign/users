@extends('dusers::layouts.base')

@section('content_header')
    <h1>{{ $permission->name }}</h1>
@stop

@section('main')
    <div class="row">
        <div class="col-lg-6">

            <form method="post" action="{{ route('dusers.permissions.update', $permission->id) }}" id="adminForm">
                <input name="_method" type="hidden" value="PUT">
                @csrf
                @include('dusers::snippets.buttons')

                <x-dcomp-box :title="__('dusers::dusers.edit')"  :collapsible="false">
                    <x-dcomp-input-string
                        name="name"
                        label="{{ ucfirst(__('dusers::dusers.users.name')) }}"
                        value="{{ old('name', $permission->name) }}"
                        :required="true"
                        :show-errors="true"
                        :errors="($errors)?? []"/>
                </x-dcomp-box>

            </form>
        </div>
    </div>
@stop


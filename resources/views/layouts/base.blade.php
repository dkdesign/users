@extends('adminlte::page')

@section('content')
    @if( session('info'))
        <div class="alert bg-info">{!! session('info') !!}</div>
    @endif
    @if( session('error'))
        <div class="alert bg-danger">{!! session('error') !!}</div>
    @endif
    @yield('main')
@append

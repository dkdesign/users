@extends('dusers::layouts.base')

@section('content_header')
    <h1>@lang('dusers::dusers.users.edit')</h1>
@append

@section('main')
    <div class="row">
        <div class="col-lg-8">
            <form id="adminForm" method="post" action="{{ route('dusers.users.update', [$user]) }}">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="id" value="{{ $user->id }}">
                @csrf
                @include('dusers::snippets.buttons')

                <x-dcomp-box :title="__('dusers::dusers.users.new')"  :collapsible="false">
                    <x-dcomp-input-string
                        name="name"
                        label="{{ ucfirst(__('dusers::dusers.users.name')) }}"
                        value="{{ old('name', $user->name) }}"
                        :required="true"
                        :show-errors="true"
                        :errors="($errors)?? []"/>

                    <x-dcomp-input-string
                        name="email"
                        label="{{ ucfirst(__('dusers::dusers.users.email')) }}"
                        value="{{ old('email', $user->email) }}"
                        :required="true"
                        :show-errors="true"
                        :errors="($errors)?? []"/>

                    <x-dcomp-multiselect
                        name="roles[]"
                        label="{{ ucfirst(__('dusers::dusers.roles')) }}"
                        :options="$roles"
                        :selected="old('roles[]', $user->roles()->pluck('name')->toArray())"
                    />
                </x-dcomp-box>
            </form>
        </div>
    </div>
@append

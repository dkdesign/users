@extends('dusers::layouts.base')

@section('content_header')
    <h1>@lang('dusers::dusers.users.management')</h1>
@stop

@section('main')
    <div class="row">
        <div class="col-12">
            <x-dcomp-box :title="__('dusers::dusers.users.users')" :collapsible="false">
                @can('create user')
                    <a href="{{ route('dusers.users.create') }}" class="mb-2 btn btn-xs btn-success">@lang('dusers::dusers.new')</a>
                @endcan

                <table id="usersTable" class="table table-bordered table-striped" role="grid">
                    <thead>
                    <tr role="row">
                        <th>@lang('dusers::dusers.users.name')</th>
                        <th>@lang('dusers::dusers.users.email')</th>
                        <th>@lang('dusers::dusers.roles')</th>
                        <th>@lang('dusers::dusers.permissions')</th>
                        <th style="width: 150px;">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($users->count())
                        @foreach ($users as $user)
                            <tr data-entry-id="{{ $user->id }}">
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @foreach ($user->roles()->pluck('name') as $role)
                                        <span class="badge badge-primary">{{ $role }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach ($user->permissions()->pluck('name') as $permission)
                                        <span class="badge badge-secondary">{{ $permission }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{ route('dusers.users.edit',[$user->id]) }}" class="btn btn-xs btn-info">@lang('dusers::dusers.edit')</a>
                                    <form style="display: inline-block;" method="post" action="{{ route('dusers.users.destroy', $user->id) }}" onsubmit="return confirm('{{ __('dusers::dusers.users.delete_sure') }}');">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" value="@lang('dusers::dusers.delete')" class="btn btn-xs btn-danger">
                                    </form>
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('dusers::dusers.users.no_items')</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </x-dcomp-box>
            @can('manage permissions')
                @include('dusers::permissions.index')
            @endcan
        </div>
    </div>
@append

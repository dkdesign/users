@extends('dusers::layouts.base')

@section('content_header')
    <h1>@lang('dusers::dusers.users.new')</h1>
@append

@section('main')
    <div class="row">
        <div class="col-lg-8">
            <form id="adminForm" method="post" action="{{ route('dusers.users.store') }}">
                @csrf
                @include('dusers::snippets.buttons')

                <x-dcomp-box :title="__('dusers::dusers.users.new')"  :collapsible="false">
                    <x-dcomp-input-string
                        name="name"
                        label="{{ ucfirst(__('dusers::dusers.users.name')) }}"
                        value="{{ old('name') }}"
                        :required="true"
                        :show-errors="true"
                        :errors="($errors)?? []"/>

                    <x-dcomp-input-string
                        name="email"
                        label="{{ ucfirst(__('dusers::dusers.users.email')) }}"
                        value="{{ old('email') }}"
                        :required="true"
                        :show-errors="true"
                        :errors="($errors)?? []"
                    />

                    <x-dcomp-input-string
                        type="password"
                        name="password"
                        label="{{ ucfirst(__('dusers::dusers.users.password')) }}"
                        :show-errors="true"
                        :errors="($errors)?? []"
                    />

                    <x-dcomp-input-string
                        type="password"
                        name="password_confirmation"
                        label="{{ ucfirst(__('dusers::dusers.users.confirm_password')) }}"
                        :show-errors="true"
                        :errors="($errors)?? []"
                    />

                    <x-dcomp-multiselect
                        name="roles[]"
                        label="{{ ucfirst(__('dusers::dusers.roles')) }}"
                        :options="$roles"
                        :selected="old('roles[]')"
                    />
                </x-dcomp-box>
            </form>
        </div>
    </div>
@append

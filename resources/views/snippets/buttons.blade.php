@php
    $toolbar = [
        [
            'label' => ucfirst(__('dusers::dusers.cancel')),
            'class' => 'default',
            'href' => route('dusers.users.index')
        ]
    ];
@endphp

<x-dcomp-submit-buttons :extra-buttons="$toolbar"/>

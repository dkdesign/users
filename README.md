# Manage users in a laravel application, including roles and permissions

[![Latest Version on Packagist](https://img.shields.io/packagist/v/deka/deka-users.svg?style=flat-square)](https://packagist.org/packages/deka/deka-users)
[![GitHub Tests Action Status](https://img.shields.io/github/workflow/status/deka/deka-users/run-tests?label=tests)](https://github.com/deka/deka-users/actions?query=workflow%3ATests+branch%3Amaster)
[![Total Downloads](https://img.shields.io/packagist/dt/deka/deka-users.svg?style=flat-square)](https://packagist.org/packages/deka/deka-users)


This is where your description should go. Limit it to a paragraph or two. Consider adding a small example.

## Installation

You can install the package via composer:

```bash
composer require deka/deka-users
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="Deka\DekaUsers\DekaUsersServiceProvider" --tag="migrations"
php artisan migrate
```

You can publish the config file with:
```bash
php artisan vendor:publish --provider="Deka\DekaUsers\DekaUsersServiceProvider" --tag="config"
```

## Usage

This package contains route endpoints for crud integration on user, roles and permissions, to be integrated in a laravel app using adminLTE
The named route to reference: 'dusers.users'

First migrate and then give yourself the role of 'super user' to be able to visit the routes. Other users can eb added with the permission 'manage users', to give them rights to visit and edit users as well.
In order for others to also have rights to change roles and permisisons add permission 'manage permissions' to their account.

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](.github/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Dirk Kokx](https://github.com/DirkKokx)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

# Security Policy

If you discover any security related issues, please email info@deka-webdesign.nl instead of using the issue tracker.

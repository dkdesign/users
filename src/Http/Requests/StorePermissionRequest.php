<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 22/01/2021
 * Time: 20:22
 */

namespace Deka\DekaUsers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StorePermissionRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::user()->can('manage users');
    }

    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }
}

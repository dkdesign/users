<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 22/01/2021
 * Time: 15:35
 */

namespace Deka\DekaUsers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StoreUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can('manage users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arr = [
            'name' => 'required',
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['nullable', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])((?=.*[0-9])|(?=.*[!$%&*#?])).*$/', 'confirmed'],
        ];

        if ($id = request()->get('id', false)) {
            $arr['email'] = [
                'required',
                'email',
                Rule::unique('users')->ignore($id),
            ];
        }

        return $arr;
    }

    public function messages()
    {
        return [
            'password.regex' => __('dusers::dusers.invalid.password'),
        ];
    }
}

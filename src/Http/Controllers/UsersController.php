<?php

namespace Deka\DekaUsers\Http\Controllers;

use Deka\DekaUsers\Helpers\DekaUsersHelper;
use Deka\DekaUsers\Http\Requests\StoreUsersRequest;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;

class UsersController extends Controller
{
    public function index()
    {
        $users = DekaUsersHelper::getUsers();

        $roles = [];
        $permissions = [];

        if (Gate::allows('manage permissions')) {
            $roles = DekaUsersHelper::getRoles();
            $permissions = Permission::get();
        }

        return view('dusers::users.index', compact('users', 'roles', 'permissions'));
    }

    public function create()
    {
        $roles = DekaUsersHelper::getRolesCursor();

        return view('dusers::users.create', compact('roles'));
    }

    public function store(StoreUsersRequest $request)
    {
        $input = $request->all();
        $usermodel = config('deka-users.user-model');

        if (! isset($input['password']) || ! $input['password']) {
            $password = DekaUsersHelper::generatePassword();
        } else {
            $password = $input['password'];
        }
        $input['password'] = bcrypt($password);

        $user = $usermodel::create($input);
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);

        if (config('deka-users.notify-on-create')) {
            if ($notification = config('deka-users.notifications.create')) {
                try {
                    $user->notify(new $notification($password));
                } catch (\Exception $e) {
                    Log::error('New user not notified: ' . $e->getMessage());
                }
            }
        }

        return $this->redirectSwitch($request, $user, __('dusers::dusers.form.item_created_successfully'));
    }

    public function edit($id)
    {
        $user = config('deka-users.user-model')::findOrFail($id);
        $roles = DekaUsersHelper::getRolesCursor();

        return view('dusers::users.edit', compact('user', 'roles'));
    }

    public function update(StoreUsersRequest $request, $id)
    {
        $user = config('deka-users.user-model')::findOrFail($id);

        $input = $request->all();
        if (isset($input['password'])) {
            $input['password'] = bcrypt($input['password']);
        }

        $user->update($input);

        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);

        return $this->redirectSwitch($request, $user, __('dusers::dusers.form.item_updated_successfully'));
    }

    public function destroy($id)
    {
        $user = config('deka-users.user-model')::find($id);

        if (! $user) {
            return redirect()->route('dusers.users.index')->with('info', __('dusers::dusers.form.item_deletion_failed'));
        }

        $user->delete();

        return redirect()->route('dusers.users.index')->with('info', __('dusers::dusers.form.item_deleted_successfully'));
    }

    /**
     * @param StoreUsersRequest $request
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectSwitch(StoreUsersRequest $request, $user, String $feedback): \Illuminate\Http\RedirectResponse
    {
        if ($request->get('submit')) {
            return redirect()->route('dusers.users.edit', [$user])->with('info', $feedback);
        }

        return redirect()->route('dusers.users.index')->with('info', $feedback);
    }
}

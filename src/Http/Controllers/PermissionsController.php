<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 21/01/2021
 * Time: 16:36
 */

namespace Deka\DekaUsers\Http\Controllers;

use Deka\DekaUsers\Http\Requests\StorePermissionRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Permission;

class PermissionsController extends Controller
{
    public function create()
    {
        return view('dusers::permissions.create');
    }

    public function store(StorePermissionRequest $request)
    {
        $permission = Permission::create($request->all());

        return $this->redirectSwitch($request, $permission, __('dusers::dusers.form.item_created_successfully'));
    }

    public function edit(Permission $permission)
    {
        return view('dusers::permissions.edit', compact('permission'));
    }

    public function update(StorePermissionRequest $request, Permission $permission)
    {
        $permission->update($request->all());

        return $this->redirectSwitch($request, $permission, __('dusers::dusers.form.item_updated_successfully'));
    }

    public function destroy(Permission $permission)
    {
        $permission->delete();

        return redirect()
            ->route('dusers.users.index')
            ->with('info', __('dusers::dusers.form.item_deleted_successfully'));
    }

    protected function redirectSwitch(StorePermissionRequest $request, Model $permission, string $feedback): \Illuminate\Http\RedirectResponse
    {
        if ($request->get('submit')) {
            return redirect()
                ->route('dusers.permissions.edit', [$permission])
                ->with('info', $feedback);
        }

        return redirect()
            ->route('dusers.users.index')
            ->with('info', $feedback);
    }
}

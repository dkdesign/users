<?php

namespace Deka\DekaUsers\Http\Controllers;

use Deka\DekaUsers\Http\Requests\StoreRoleRequest;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    public function create()
    {
        $permissions = Permission::pluck('name', 'name')->toArray();

        return view('dusers::roles.create', compact('permissions'));
    }

    public function store(StoreRoleRequest $request)
    {
        $role = Role::create($request->all());
        $permissions = $request->input('permissions') ? $request->input('permissions') : [];

        if (count($permissions)) {
            $role->givePermissionTo($permissions);
        }

        return $this->redirectSwitch($request, $role,  __('dusers::dusers.form.item_created_successfully'));
    }

    public function edit(Role $role)
    {
        $permissions = Permission::pluck('name', 'name')->toArray();

        return view('dusers::roles.edit', compact('role', 'permissions'));
    }

    public function update(StoreRoleRequest $request, Role $role)
    {
        $role->update($request->all());
        $permissions = $request->get('permissions') ? $request->get('permissions') : [];
        $role->syncPermissions($permissions);

        return $this->redirectSwitch($request, $role,  __('dusers::dusers.form.item_updated_successfully'));
    }

    public function destroy(Role $role)
    {
        $role->delete();

        return redirect()
            ->route('dusers.users.index')
            ->with('info', __('dusers::dusers.form.item_deleted_successfully'));
    }

    protected function redirectSwitch(StoreRoleRequest $request, Role $role, $feedback): \Illuminate\Http\RedirectResponse
    {
        if ($request->get('submit')) {
            return redirect()
                ->route('dusers.roles.edit', [$role])
                ->with('info', $feedback);
        }

        return redirect()
            ->route('dusers.users.index')
            ->with('info', $feedback);
    }
}

<?php

namespace Deka\DekaUsers;

use Deka\DekaUsers\Http\Controllers\PermissionsController;
use Deka\DekaUsers\Http\Controllers\RolesController;
use Deka\DekaUsers\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class DekaUsersServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/deka-users.php' => config_path('deka-users.php'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/../resources/views' => base_path('resources/views/vendor/deka-users'),
            ], 'views');

            $migrationFileName = 'create_permission_tables.php';
            if (! $this->migrationFileExists($migrationFileName)) {
                $this->publishes([
                    __DIR__ . "/../database/migrations/{$migrationFileName}.stub" => database_path('migrations/' . date('Y_m_d_His', time()) . '_' . $migrationFileName),
                ], 'migrations');
            }
        }

        $this->loadTranslationsFrom(__DIR__. '/../resources/lang', 'dusers');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'dusers');
        $this->setRoutes();

        // Implicitly grant "Webmaster" role all permissions
        Gate::before(function ($user, $ability) {
            return ($user->hasRole('super user'))? true : null;
        });
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/deka-users.php', 'deka-users');
    }

    public function setRoutes()
    {
        Route::macro('deka_users', function (string $prefix) {
            Route::prefix($prefix)->middleware(['web', 'auth', 'can:manage users'])->group(function () {
                Route::resource('users', UsersController::class, ['as' => 'dusers', 'except' => 'show']);

                Route::middleware('can:manage permissions')->group(function () {
                    Route::resource('permissions', PermissionsController::class, ['except' => ['index', 'show'], 'as' => 'dusers']);
                    Route::resource('roles', RolesController::class, ['except' => ['index', 'show'], 'as' => 'dusers']);
                });
            });
        });
    }

    public static function migrationFileExists(string $migrationFileName): bool
    {
        $len = strlen($migrationFileName);
        foreach (glob(database_path("migrations/*.php")) as $filename) {
            if ((substr($filename, -$len) === $migrationFileName)) {
                return true;
            }
        }

        return false;
    }
}

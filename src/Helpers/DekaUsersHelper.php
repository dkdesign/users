<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 22/01/2021
 * Time: 14:49
 */

namespace Deka\DekaUsers\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class DekaUsersHelper
{
    public static function getRolesCursor()
    {
        return self::getRoles()->pluck('name', 'name');
    }

    public static function getRoles()
    {
        if (Auth::user()->hasRole('super user')) {
            return Role::get();
        } else {
            return Role::where('name', '<>', 'super user')->get();
        }
    }

    public static function generatePassword()
    {
        $special = ['!','$','%','&','*','#','?'];
        $pw = 'nope';
        while (! preg_match('/^.*(?=.{3,})(?=.*[a-zA-Z])((?=.*[0-9])|(?=.*[!$%&*#?])).*$/', $pw)) {
            $pw = Str::random(10);
            $char = $special[rand(0, 6)];
            $pw = substr_replace($pw, $char, rand(0, 9), 0);
        }

        return $pw;
    }

    public static function getUsers()
    {
        $model = config('deka-users.user-model');
        $hideSuper = config('deka-users.hide-super');
        if ($hideSuper) {
            return $model::whereHas('roles', function ($q) {
                return $q->where('name', '<>', 'super user');
            })
                ->orWhereDoesntHave('roles')
                ->get();
        } else {
            return $model::get();
        }
    }
}

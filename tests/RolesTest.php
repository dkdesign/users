<?php

namespace Deka\DekaUsers\Tests;

use Deka\DekaUsers\Tests\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;

class RolesTest extends TestCase
{
    use WithFaker;

    protected function managerRequest()
    {
        $user = factory(User::class)->create();
        $user->givePermissionTo(['manage users', 'manage permissions']);

        return $this->actingAs($user);
    }

    /**
     * @test
     */
    public function a_role_creation_form_can_be_opened()
    {
        $this->managerRequest()->get('deka/roles/create')
            ->assertStatus(200)
            ->assertSee('form')
        ;
    }

    /**
     * @test
     */
    public function a_new_role_can_be_stored()
    {
        $postdata = factory(Role::class)->make()->toArray();
        $postdata['submitclose'] = 1;

        $this->managerRequest()->followingRedirects()->post('deka/roles', $postdata)
            ->assertStatus(200)
            ->assertSee($postdata['name'])
            ->assertSee(__('dusers::dusers.form.item_created_successfully'))
        ;
    }

    /**
     * @test
     */
    public function a_new_role_can_be_stored_with_permissions()
    {
        $postdata = factory(Role::class)->make()->toArray();
        $postdata['permissions'] = [
            'manage users',
        ];

        $this->managerRequest()->followingRedirects()->post('deka/roles', $postdata)
            ->assertStatus(200)
            ->assertSee($postdata['name'])
            ->assertSee(__('dusers::dusers.form.item_created_successfully'))
        ;

        $role = Role::orderBy('id', 'DESC')->first();
        $this->assertCount(1, $role->permissions);
    }

    /**
     * @test
     */
    public function a_role_edit_form_can_be_opened()
    {
        $role = factory(Role::class)->create();
        $this->managerRequest()->get(route('dusers.roles.edit', [$role->id]))
            ->assertStatus(200)
            ->assertSee('form')
            ->assertSee($role->name)
        ;
    }

    /**
     * @test
     */
    public function a_role_can_be_updated()
    {
        $role = factory(Role::class)->create();
        $postdata = $role->toArray();
        $postdata['permissions'] = [
            'manage users',
        ];
        $postdata['submit'] = 1;
        $this->managerRequest()->followingRedirects()->put(route('dusers.roles.update', [$role->id]), $postdata)
            ->assertStatus(200)
            ->assertSee(__('dusers::dusers.form.item_updated_successfully'))
        ;
    }

    /**
     * @test
     */
    public function a_role_can_be_deleted()
    {
        $role = factory(Role::class)->create();
        $this->managerRequest()->followingRedirects()->delete(route('dusers.roles.destroy', $role))
            ->assertStatus(200)
            ->assertSee(__('dusers::dusers.form.item_deleted_successfully'))
        ;
    }

    /**
     * @test
     */
    public function deleting_a_role_that_does_not_exitst_returns_eror()
    {
        $this->managerRequest()->delete(route('dusers.roles.destroy', ['role' => 450]))
            ->assertStatus(404)
        ;
    }
}

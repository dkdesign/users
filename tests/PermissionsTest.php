<?php

namespace Deka\DekaUsers\Tests;

use Deka\DekaUsers\Tests\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Permission;

class PermissionsTest extends TestCase
{
    use WithFaker;

    protected function managerRequest()
    {
        $user = factory(User::class)->create();
        $user->givePermissionTo(['manage users', 'manage permissions']);

        return $this->actingAs($user);
    }

    /**
     * @test
     */
    public function a_permission_creation_form_can_be_opened()
    {
        $this->managerRequest()->get('deka/permissions/create')
            ->assertStatus(200)
            ->assertSee('form')
        ;
    }

    /**
     * @test
     */
    public function a_new_permission_can_be_stored()
    {
        $this->withoutExceptionHandling();
        $postdata = factory(Permission::class)->make()->toArray();
        $postdata['submitclose'] = 1;

        $this->managerRequest()->followingRedirects()->post('deka/permissions', $postdata)
            ->assertStatus(200)
            ->assertSee($postdata['name'])
            ->assertSee(__('dusers::dusers.form.item_created_successfully'))
        ;
    }

    /**
     * @test
     */
    public function a_permission_edit_form_can_be_opened()
    {
        $permission = factory(Permission::class)->create();
        $this->managerRequest()->get(route('dusers.permissions.edit', [$permission->id]))
            ->assertStatus(200)
            ->assertSee('form')
            ->assertSee($permission->name)
        ;
    }

    /**
     * @test
     */
    public function a_permission_can_be_updated()
    {
        $permission = factory(Permission::class)->create();
        $postdata = $permission->toArray();
        $postdata['name'] = 'newName';
        $postdata['submit'] = 1;
        $this->managerRequest()->followingRedirects()->put(route('dusers.permissions.update', [$permission->id]), $postdata)
            ->assertStatus(200)
            ->assertSee('newName')
            ->assertSee(__('dusers::dusers.form.item_updated_successfully'))
        ;
    }

    /**
     * @test
     */
    public function a_permission_can_be_deleted()
    {
        $permission = factory(Permission::class)->create();
        $this->managerRequest()->followingRedirects()->delete(route('dusers.permissions.destroy', $permission))
            ->assertStatus(200)
            ->assertSee(__('dusers::dusers.form.item_deleted_successfully'))
        ;
    }

    /**
     * @test
     */
    public function deleting_a_permission_that_does_not_exitst_returns_eror()
    {
        $this->managerRequest()->delete(route('dusers.permissions.destroy', ['permission' => 450]))
            ->assertStatus(404)
        ;
    }
}

<?php

namespace Deka\DekaUsers\Tests;

use Deka\DekaUsers\Tests\Models\User;
use Illuminate\Foundation\Testing\WithFaker;

class UsersTest extends TestCase
{
    use WithFaker;

    protected function managerRequest()
    {
        $user = factory(User::class)->create();
        $user->givePermissionTo('manage users');

        return $this->actingAs($user);
    }

    /**
     * @test
     */
    public function a_user_creation_form_can_be_opened()
    {
        $this->managerRequest()->get('deka/users/create')
            ->assertStatus(200)
            ->assertSee('form')
        ;
    }

    /**
     * @test
     */
    public function a_new_user_can_be_stored()
    {
        $postdata = factory(User::class)->make()->toArray();
        $postdata['password_confirmation'] = $postdata['password'] = 'test-A123';
        $postdata['submitclose'] = 1;

        $this->managerRequest()->followingRedirects()->post('deka/users', $postdata)
            ->assertStatus(200)
            ->assertSee($postdata['email'])
            ->assertSee(__('dusers::dusers.form.item_created_successfully'))
        ;
    }

    /**
     * @test
     */
    public function a_password_is_created_if_none_is_given()
    {
        $postdata = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
        ];

        $this->managerRequest()->followingRedirects()->post('deka/users', $postdata)
            ->assertStatus(200)
            ->assertSee($postdata['email'])
            ->assertSee(__('dusers::dusers.form.item_created_successfully'));

        $this->assertEquals(1, User::where('email', $postdata['email'])->whereNotNull('password')->count());
    }

    /**
     * @test
     */
    public function a_user_edit_form_can_be_opened()
    {
        $user = factory(User::class)->create();
        $this->managerRequest()->get(route('dusers.users.edit', [$user->id]))
            ->assertStatus(200)
            ->assertSee('form')
            ->assertSee($user->email)
        ;
    }

    /**
     * @test
     */
    public function a_user_can_be_updated()
    {
        $user = factory(User::class)->create();
        $postdata = $user->toArray();
        $postdata['email'] = $this->faker->email;
        $this->managerRequest()->followingRedirects()->put(route('dusers.users.update', [$user->id]), $postdata)
            ->assertStatus(200)
            ->assertSee(__('dusers::dusers.form.item_updated_successfully'))
        ;
    }

    /**
     * @test
     */
    public function a_user_can_be_deleted()
    {
        $user = factory(User::class)->create();
        $this->managerRequest()->followingRedirects()->delete(route('dusers.users.destroy', [$user->id]))
            ->assertStatus(200)
            ->assertSee(__('dusers::dusers.form.item_deleted_successfully'))
        ;
    }

    /**
     * @test
     */
    public function deleting_a_user_that_does_not_exitst_returns_eror()
    {
        $this->managerRequest()->followingRedirects()->delete(route('dusers.users.destroy', ['user' => 450]))
            ->assertStatus(200)
            ->assertSee(__('dusers::dusers.form.item_deletion_failed'))
        ;
    }
}

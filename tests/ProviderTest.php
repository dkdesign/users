<?php

namespace Deka\DekaUsers\Tests;

use Deka\DekaUsers\Tests\Models\User;
use Illuminate\Support\Facades\Config;

class ProviderTest extends TestCase
{
    protected function getSuper()
    {
        $user = factory(User::class)->create();
        $user->assignRole('super user');

        return $user;
    }

    protected function managerRequest()
    {
        $user = factory(User::class)->create();
        $user->givePermissionTo('manage users');

        return $this->actingAs($user);
    }

    /** @test */
    public function it_sets_config_user_model()
    {
        $this->assertArrayHasKey('user-model', config('deka-users'));
    }

    /**
     * @test
     */
    public function it_is_not_accessible_if_not_logged_in()
    {
        $this->json('get', 'deka/users')
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function it_is_not_accessible_for_logged_in_users_without_rights()
    {
//        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $this->actingAs($user)->json('get', 'deka/users')
            ->assertStatus(403);
    }

    /**
     * @test
     */
    public function it_has_an_index_route()
    {
        $this->managerRequest()->json('get', 'deka/users')
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function the_index_function_returns_a_table_if_there_are_users()
    {
        $this->managerRequest()->json('get', 'deka/users')
            ->assertSee('usersTable');
    }

    /**
     * @test
     */
    public function the_page_has_a_translated_title()
    {
        $title = __('dusers::dusers.users.management');
        $this->managerRequest()->json('get', 'deka/users')
            ->assertStatus(200)
            ->assertSee($title);
    }

    /**
     * @test
     */
    public function it_returns_a_user_count_on_users()
    {
        factory(User::class, 4)->create();

        $this->managerRequest()->json('get', 'deka/users')
            ->assertSee(5);
    }

    /**
     * @test
     */
    public function it_returns_a_message_when_there_are_no_users()
    {
        $user = $this->getSuper();

        $this->actingAs($user)->json('get', 'deka/users')
            ->assertSee(__('dusers::dusers.users.no_items'));
    }

    /**
     * @test
     */
    public function it_shows_the_users_name_and_email()
    {
        $user = factory(User::class)->create();

        $this->managerRequest()->json('get', 'deka/users')
            ->assertSee($user->name)
            ->assertSee($user->email)
        ;
    }

    /**
     * @test
     */
    public function it_hides_super_users()
    {
        $user = $this->getSuper();

        $this->actingAs($user)->json('get', 'deka/users')
            ->assertDontSee($user->email);
    }

    /**
     * @test
     */
    public function it_shows_super_users_on_config_false()
    {
        $user = $this->getSuper();
        Config::set('deka-users.hide-super', false);

        $this->actingAs($user)->json('get', 'deka/users')
            ->assertSee($user->email);
    }

    /**
     * @test
     */
    public function a_logged_in_user_with_permission_to_manage_permissions_sees_its_table()
    {
        $user = factory(User::class)->create();
        $user->givePermissionTo(['manage permissions', 'manage users']);

        $this->actingAs($user)->json('get', 'deka/users')
            ->assertSee('rolesTable');
    }

    /**
     * @test
     */
    public function a_logged_in_user_without_permission_to_manage_permissoins_does_not_see_its_table()
    {
        $this->managerRequest()->json('get', 'deka/users')
            ->assertDontSee('rolesTable');
    }

    /**
     * @test
     */
    public function a_logged_in_super_user_can_see_permissions_table()
    {
        $user = $this->getSuper();
        $this->actingAs($user)->json('get', 'deka/users')
            ->assertSee('rolesTable');
    }
}

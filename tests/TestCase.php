<?php

namespace Deka\DekaUsers\Tests;

use Deka\Components\ComponentsServiceProvider;
use Deka\DekaUsers\DekaUsersServiceProvider;
use Deka\DekaUsers\Tests\seeders\TestSeeder;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Route;
use JeroenNoten\LaravelAdminLte\AdminLteServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    public function setUp(): void
    {
        parent::setUp();
        $this->withFactories(__DIR__.'/../database/factories');

        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        $this->seedDB();

        Route::deka_users('deka');

        Factory::guessFactoryNamesUsing(
            fn (string $modelName) => 'Deka\\DekaUsers\\Database\\Factories\\'.class_basename($modelName).'Factory'
        );

//        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();
    }

    protected function getPackageProviders($app)
    {
        return [
            \Spatie\Permission\PermissionServiceProvider::class,
            AdminLteServiceProvider::class,
            ComponentsServiceProvider::class,
            DekaUsersServiceProvider::class,
        ];
    }

    public function getEnvironmentSetUp($app)
    {
        $app['config']->set('auth.guards', [
            'web' => [
                'driver' => 'session',
                'provider' => 'users',
            ],
        ]);
        $app['config']->set('app.locale', 'nl');
        $app['config']->set('deka-users.user-model', 'Deka\DekaUsers\Tests\Models\User');

        $app['config']->set('database.default', 'sqlite');
        $app['config']->set('database.connections.sqlite', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
            ]);

        $app['config']->set('app.key', 'base64:zwzTEehT/Ql0QjHjciISYC7lM1QEpdU/bCK8Re1pLRI=');
        $app['config']->set('app.cipher', 'AES-256-CBC');
    }

    public function seedDB()
    {
        (new TestSeeder())->run();
    }
}

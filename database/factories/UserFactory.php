<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Deka\DekaUsers\Tests\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->email,
        'password' => $faker->password,
    ];
});

<?php

return [
    /**
     * the users model FQCN
     */
    'user-model' => 'App\Models\User',

    /**
     * Whether to hide the super users from the user list.
     * This to prevent administrators from deleting the super users
     * Super users will have access to these data and can remove them
     */
    'hide-super' => true,

    /**
     * should the new user be notified of his new account?
     */
    'notify-on-create' => false,

    /**
     * definition of the notifications to be used
     */
    'notifications' => [
        'create' => 'App\Notifications\NewAccount'
    ]
];

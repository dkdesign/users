# Changelog

All notable changes to `deka-users` will be documented in this file.

## 1.0.0 - 2021-01-23

- initial release
